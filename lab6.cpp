/*
Robert Dukes
CPSC 1020, Section 01, F20
rhdukes@g.clemson.edu
Nushrat Humaira
*/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {

  srand(unsigned (time(0)));
	employee arr[10];


	for(int i = 0; i < 10; ++i){ //for loop reapeats 10 times for user inputs
		cout << "Enter the last name of employee #"<< i+1 << endl;
		cin >> arr[i].lastName;

		cout << "Enter the first name of employee #"<< i+1 << endl;
		cin >> arr[i].firstName;

		cout << "Enter the year of birth of employee #"<< i+1 << endl;
		cin >> arr[i].birthYear;

		cout << "Enter the hourly wage of employee #"<< i+1 << endl;
		cin >> arr[i].hourlyWage;
		cout << endl;
	}


	 employee* ptr = &arr[0]; //points to begining of array
	 employee* endptr = &arr[10]; //points to end of arr + 1

	 random_shuffle(ptr, endptr, myrandom); //calls on random_shuffle with my
                                          //random as function

		employee empArr[5]; //initializes array with half length as before
		for(int i = 0; i < 5; ++i){ //puts first five indices of arr into empArr
			empArr[i] = arr[i];
		}
		ptr = &empArr[0]; //begining and end + 1 of empArr
		endptr = &empArr[5];


		 sort(ptr, endptr, name_order); //sorts out empArr


		for(auto x: empArr){ // runs for every indice, printing the vals of empArr
			cout << setw(160) << right << x.lastName + ","
			+ x.firstName << endl;

			cout << setw(160) << right << x.birthYear << endl;
			cout << setw(160) << right << fixed << showpoint << setprecision(2) << x.hourlyWage
			<< endl << endl;
		}

  return 0;
}


bool name_order(const employee& lhs, const employee& rhs) {

  if(lhs.lastName >= rhs.lastName){ //if current is greater than or equal to next, return false
		return false;
	}

	return true;
}
